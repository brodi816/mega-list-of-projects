using System;

namespace MegaProject
{
	public class PigLatin : ProjectBase
	{
		private static readonly char[] _Consonants = new char[]
		{'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'w', 'v', 'x', 'y', 'z'};

		public PigLatin (string name)
			: base(name)
		{
		}

		public static string GetPigLatinString (string word)
		{
			if (word == null || word.Length == 0) {
				throw new Exception("Word not valid to convert in PigLatin");
			}

			foreach (char consonant in _Consonants) {
				if(word.ToLower()[0] == consonant) {
					string pigRet = word.Substring(1);
					pigRet += consonant + "ay";
					return pigRet;
				}
			}

			return word + "yay";
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);
			if (!args.ContainsKey ("i")) {
				Output.GlobalInstance.WriteLine("Define input string with -i input");
				return;
			}

			string input = args["i"];
			string pigLatin = GetPigLatinString(input);

			Output.GlobalInstance.WriteLine(string.Format("Input: {0}, Pig Latin: {1}", input, pigLatin));
		}
	}
}

