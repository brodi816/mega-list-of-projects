using System;

namespace LayeredConsole
{
	public struct LayeredChar
	{
		public char Char;
		public byte Layer;

		public LayeredChar(char Char = '\0', byte Layer = 0)
		{
			this.Char = Char;
			this.Layer = Layer;
		}
	}
}

