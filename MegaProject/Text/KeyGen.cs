using System;

namespace MegaProject
{
	public class KeyGen : ProjectBase
	{
		public KeyGen (string name)
			: base(name)
		{
		}

		public static short[] CreateRandomXCoords (int count)
		{
			short[] xCoords = new short[count];

			Random r = new Random();

			for (int i = 0; i < count; i++) {
				xCoords[i] = Convert.ToInt16(r.Next(short.MinValue, short.MaxValue));
			}

			return xCoords;
		}

		public static int GetChecksum24 (string value)
		{
			//Calculates a 24bit checksum
			uint checksum = 0;
			foreach (char c in value) {
				byte shift = (byte)(checksum & 0xF);

				checksum = checksum ^ c;
				checksum = (checksum << shift) | (checksum >> (32-shift));
			}
			//Use a mask to extract only 24 bits
		    return (int)(checksum & 0xFFFFFF);
		}

		public static sbyte GetYFromX (short x)
		{
			//Y = ((sin((x/256)^2)*(cos(x)^2)) + cos((x/64)^4))
			//(sin((x/16))*(cos(x/8))) + (cos(x/256)*64*x)
			//((sin((x/2)/32)^2)*(cos(x/4)))*16+x/32 + (sin(x/8)*4)
			//(((sin((x/2)/32)^2)*(cos(x/4)))*8 + (sin(x/8)*4))*20


			double mul1d = Math.Sin((x/2)/32);
			mul1d *= mul1d;
			double mul2d = Math.Cos(x/4)*8;
			double add1d = Math.Sin(x/8)*4;

			return Convert.ToSByte(((mul1d*mul2d) + add1d)*8);
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);

			if (args.ContainsKey ("g")) {

				//Generate random x corrds and fetch the y pos for them
				short[] xCoords = CreateRandomXCoords(4);
				sbyte[] yCoords = new sbyte[xCoords.Length];

				for(int i = 0; i < xCoords.Length; i++) {
					yCoords[i] = GetYFromX(xCoords[i]);
				}

				string[] codeArray = new string[xCoords.Length];


				//Convert the code to hex
				for(int i = 0; i < xCoords.Length; i++) {
					string xCoord = xCoords[i].ToString("X4");

					string yCoord = yCoords[i].ToString("X2");

					codeArray[i] = xCoord + yCoord;
				}

				//Combine the hex code array into a outputable string
				string code = "";
				for(int i = 0; i < codeArray.Length; i++)
				{
					if(i != 0) {
						code += "-";
					}
					code += codeArray[i];
				}

				string codeChecksum = GetChecksum24(code).ToString("X6");

				code += "-" + codeChecksum;

				Output.GlobalInstance.WriteLine("Generated Code: " + code);

			} else if (args.ContainsKey ("v")) {
				string input = args["v"];


				int lastSplitIndex = input.LastIndexOf('-');
				string checksum = input.Substring(lastSplitIndex + 1);
				string codeNoChecksum = input.Substring(0, lastSplitIndex);
				string[] coordsArray = codeNoChecksum.Split('-');


				string genChecksum = GetChecksum24(codeNoChecksum).ToString("X6");
				
				
				if(!genChecksum.Equals(checksum)) {
					Output.GlobalInstance.WriteLine("Checksum failed for code");
					return;
				}

				//Validating the coords in the code
				for(int i = 0; i < coordsArray.Length; i++) {
					string coords = coordsArray[i];
					Output.GlobalInstance.WriteLine(
						String.Format("coords hex: ({0},{1})", coords.Substring(0,4), coords.Substring(4,2)));

					//Convert the coords to usable types
					short xCoord = Convert.ToInt16(coords.Substring(0,4), 16);
					sbyte yCoord = Convert.ToSByte(coords.Substring(4, 2), 16);

					Output.GlobalInstance.WriteLine(
						String.Format("coords numeric: ({0},{1})", xCoord, yCoord));

					//Validate the coords
					if(GetYFromX(xCoord) != yCoord) {
						Output.GlobalInstance.WriteLine(String.Format("Code {0} failed coord function check", i));
						return;
					}
				}

				Output.GlobalInstance.WriteLine(String.Format("The code {0} is valid", input));
			} else {
				Output.GlobalInstance.WriteLine("Use -g to generate or -v to verify a code");
			}
		}
	}
}

