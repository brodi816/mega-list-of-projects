using System;
using System.IO;
using System.Collections.Generic;

namespace LayeredConsole
{
	public class LinkedLineFile
	{
		private LinkedList<String> _Text = new LinkedList<String>();

		private int _RefRow = 0;
		private LinkedListNode<String> _RefNode = null;

		public LinkedLineFile()
		{
			_RefNode = new LinkedListNode<String> ("");
			_Text.AddFirst (_RefNode);
		}

		public LinkedLineFile (string[] lines)
		{
			readFile (lines);
		}

		private void readFile(string [] lines)
		{
			_Text.Clear ();
			foreach (string line in lines) {
				_Text.AddLast (line);
			}

			_RefRow = 0;
			_RefNode = _Text.First;
		}

		public LinkedListNode<String> GetNode(int row)
		{
			if (row < 0 || row >= _Text.Count) {
				throw new ArgumentOutOfRangeException ("row", row, "Row is out of the file bounds");
			}

			LinkedListNode<String> retNode = _Text.First;
			for (int i = 0; i < row; i++) {
				retNode = retNode.Next;
			}
			return retNode;
		}

		public LinkedListNode<String> GetNodeFromRefRow(int row)
		{
			if (row < 0 || row >= _Text.Count) {
				throw new ArgumentOutOfRangeException ("row", row, "Row is out of the file bounds");
			}

			LinkedListNode<String> retNode = _RefNode;
			//move forwards/backwards on linked list to find the row to return
			if (row > _RefRow) {
				for (int i = 0; i < row - _RefRow; i++) {
					retNode = retNode.Next;
				}
			} else if (row < _RefRow) {
				for (int i = 0; i < _RefRow - row; i++) {
					retNode = retNode.Previous;
				}
			}
			return retNode;
		}

		public void RemoveLine(int row)
		{
			if (row < 0 || row >= _Text.Count) {
				throw new ArgumentOutOfRangeException ("row", row, "Row is out of the file bounds");
			}

			if (_RefRow >= row) {
				//decrease the current reference node to keep index in sync with node
				if (_RefNode.Previous != null) {
					_RefNode = _RefNode.Previous;
					_RefRow--;
				} else if (_Text.Count == 1) {
					_RefRow = 0;
					_RefNode.Value = "";
					return;
				}
			}

			_Text.Remove (GetNodeFromRefRow (row));
		}

		public void AddRow(int rowAfter, string text)
		{
			if (rowAfter >= _Text.Count) {
				_Text.AddLast (text);
			} else {
				_Text.AddBefore (GetNodeFromRefRow (rowAfter), text);
				//increase current reference index if line is added before reference
				if (rowAfter <= _RefRow) {
					_RefRow++;
				}
			}
		}

		public int RefRow
		{
			get {
				return _RefRow;
			}
			set {
				if (value < 0 || value >= _Text.Count) {
					throw new ArgumentOutOfRangeException ("row", value, "Row is out of the file bounds");
				}

				_RefNode = GetNodeFromRefRow (value);
				_RefRow = value;
			}
		}

		public LinkedListNode<String> RefNode
		{
			get
			{
				return _RefNode;
			}
		}

		public string[] Lines
		{
			get {
				string[] lines = new string[_Text.Count];
				_Text.CopyTo (lines, 0);
				return lines;
			}
		}

		public LinkedList<String> LinkedLines
		{
			get {
				return _Text;
			}
		}

		public int LineCount
		{
			get
			{
				return _Text.Count;
			}
		}
	}
}

