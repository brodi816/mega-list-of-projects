using System;
using System.Collections.Generic;

namespace MegaProject
{
	public class ChangeReturn : ProjectBase
	{
		enum Change
		{
			QUARTER,
			DIME,
			NICKEL,
			PENNY
		}

		private static readonly Change[] CoinHighToLow = new Change[] 
				{Change.QUARTER, Change.DIME, Change.NICKEL, Change.PENNY};

		private static readonly Dictionary<Change, int> CoinValue = new Dictionary<Change, int>();

		public ChangeReturn (string name)
			: base(name)
		{
			CoinValue[Change.QUARTER] = 25;
			CoinValue[Change.DIME] = 10;
			CoinValue[Change.NICKEL] = 5;
			CoinValue[Change.PENNY] = 1;
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);

			if (args.ContainsKey ("c") && args.ContainsKey ("g")) {
				double cost = Double.Parse(args["c"]);
				double moneyGiven = Double.Parse(args["g"]);
				double change = moneyGiven - cost;

				int coinChangeOrig = (int)Math.Ceiling((change - Math.Floor(change)) * 100);
				int coinChange = coinChangeOrig;

				Output.GlobalInstance.WriteLine("Coin Change: " + coinChange);

				Dictionary<Change, int> coinChangeIndividual = new Dictionary<Change, int>();

				for(int i = 0; i < CoinHighToLow.Length; i++) {
					coinChangeIndividual[CoinHighToLow[i]] = 0;
				}


				while(coinChange > 0) {
					for(int i = 0; i < CoinHighToLow.Length; i++) {
						Change changeKey = CoinHighToLow[i];
						if(coinChange >= CoinValue[changeKey]) {
							coinChangeIndividual[changeKey] = coinChangeIndividual[changeKey]+1;
							coinChange -= CoinValue[changeKey];
						}
					}
				}


				for(int i = 0; i < CoinHighToLow.Length; i++) {
					Change key = CoinHighToLow[i];
					int amount = 0;
					if(coinChangeIndividual.ContainsKey(key)) {
						amount = coinChangeIndividual[key];
					}
					Output.GlobalInstance.WriteLine(amount + " " + key.ToString());
				}

			} else {
				Output.GlobalInstance.WriteLine("Give the cost with -c and moeny given with g");
			}
		}
	}
}

