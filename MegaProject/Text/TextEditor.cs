using System;
using System.Threading;
using LayeredConsole;

namespace MegaProject
{
	public class TextEditor : ProjectBase
	{
		public TextEditor (string name)
			: base(name)
		{
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);

			int CursorRow = 0;
			int CursorX = 0;


			LayeredConsole.LayeredConsole console = new LayeredConsole.LayeredConsole();
			LinkedLineFile currentFile = new LinkedLineFile ();
			FileConsoleRenderer fileRenderer = new FileConsoleRenderer ();
			fileRenderer.File = currentFile;

			console.Renderers.Add (fileRenderer);
		
			bool running = true;
			ulong tick = 0;

			while (running) {
				Thread.Sleep (10);

				while (Console.KeyAvailable) {


					bool keyIntercepted = false;
					ConsoleKeyInfo key = Console.ReadKey (true);

					//special case keys
					switch(key.Key) {
					case ConsoleKey.Escape:
						running = false;
						keyIntercepted = true;
						break;
					case ConsoleKey.Backspace:
						keyIntercepted = true;

						if (CursorX == 0) {
							//add string onto previous row
							if (CursorRow != 0) {
								CursorX = currentFile.GetNodeFromRefRow (CursorRow - 1).Value.Length;
								currentFile.GetNodeFromRefRow (CursorRow - 1).Value += currentFile.RefNode.Value;
								currentFile.RemoveLine (CursorRow);
								CursorRow--;
							}
							break;
						}

						currentFile.RefNode.Value = currentFile.RefNode.Value.Remove (CursorX - 1, 1);
						CursorX--;

						break;
					case ConsoleKey.Enter:
						keyIntercepted = true;

						currentFile.AddRow (CursorRow + 1, "");
						if (CursorX < currentFile.GetNodeFromRefRow (CursorRow).Value.Length) {
							currentFile.GetNodeFromRefRow (CursorRow + 1).Value = currentFile.GetNodeFromRefRow (CursorRow).Value.Substring (CursorX);
							currentFile.GetNodeFromRefRow (CursorRow).Value = currentFile.GetNodeFromRefRow (CursorRow).Value.Remove (CursorX);
						}
						CursorX = 0;
						CursorRow++;
						break;
					//cursor movement
					case ConsoleKey.LeftArrow:
						if (CursorX > 0) {
							CursorX--;
						} else if(CursorRow > 0) {
							CursorX = currentFile.GetNodeFromRefRow (CursorRow - 1).Value.Length;
							CursorRow--;
						}

						keyIntercepted = true;
						break;
					case ConsoleKey.RightArrow:
						if (CursorX < currentFile.GetNodeFromRefRow (CursorRow).Value.Length) {
							CursorX++;
						} else if (CursorRow < currentFile.LineCount - 1) {
							CursorX = 0;
							CursorRow++;
						}
						keyIntercepted = true;
						break;
					case ConsoleKey.UpArrow:
						if (CursorRow > 0) {
							CursorRow--;

							//move the cursor left if it's out of bounds
							if (currentFile.GetNodeFromRefRow (CursorRow).Value.Length < CursorX) {
								CursorX = currentFile.GetNodeFromRefRow (CursorRow).Value.Length;
							}
						}
						keyIntercepted = true;
						break;
					case ConsoleKey.DownArrow:
						if (CursorRow < (currentFile.LineCount - 1) ) {
							CursorRow++;

							//move the cursor left if it's out of bounds
							if (currentFile.GetNodeFromRefRow (CursorRow).Value.Length < CursorX) {
								CursorX = currentFile.GetNodeFromRefRow (CursorRow).Value.Length;
							}
						}
						keyIntercepted = true;
						break;
					}

					currentFile.RefRow = CursorRow;

					if (!keyIntercepted) {
						currentFile.RefNode.Value = currentFile.RefNode.Value.Insert(CursorX, key.KeyChar.ToString());
						CursorX++;
					}


					Console.SetCursorPosition (fileRenderer.GetRenderCursorPos(CursorRow, CursorX), 
					                           CursorRow);
				}

				if ((tick % 5) == 0) {
					console.OutputToConsole ();
				}
				tick++;
			}
		}
	}
}

