using System;

namespace MegaProject
{
	public class TileCost : MegaProject.ProjectBase
	{
		public TileCost (string name)
			: base(name)
		{
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);

			if (args.ContainsKey ("c") && args.ContainsKey ("w") && args.ContainsKey ("h")) {
				double cost = Double.Parse(args["c"]);
				double width = Double.Parse(args["w"]);
				double height = Double.Parse(args["h"]);
				Output.GlobalInstance.WriteLine(String.Format("Cost for area {0} is {1}", width * height, width * height * cost));
			} else {
				Output.GlobalInstance.WriteLine("use -c to define the cost per square area and -w and -h for width and height");
			}
		}
	}
}

