using System;

namespace MegaProject
{
	public class Palindrome : ProjectBase
	{
		public Palindrome (string name)
			: base(name)
		{
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);

			if (!args.ContainsKey ("i")) {
				Output.GlobalInstance.WriteLine ("Define input string with -i input");
				return;
			}

			string input = args ["i"];
			string reverse = ReverseString.Reverse (input.ToLower ());

			Output.GlobalInstance.WriteLine(String.Format("Input: {0}, Reverse: {1}", input, reverse));
			if (reverse.Equals (input.ToLower ())) {
				Output.GlobalInstance.WriteLine(String.Format("{0} is a palindrome", input));
			} else {
				Output.GlobalInstance.WriteLine(String.Format("{0} is not a palindrome", input));
			}
		}
	}
}

