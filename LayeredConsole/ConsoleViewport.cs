using System;

namespace LayeredConsole
{
	public struct ConsoleViewport
	{
		public int X, Y, Width, Height;

		public ConsoleViewport (int width, int height)
		{
			this.X = 0;
			this.Y = 0;
			this.Width = width;
			this.Height = height;
		}

		public void ChangePosition(int x, int y)
		{
			this.X = x;
			this.Y = y;
		}

		public void ChangeSize (int width, int height)
		{
			this.Width = width;
			this.Height = height;
		}

	}
}

