using System;
using System.IO;
using System.Text;

namespace MegaProject
{
	public class Output
	{
		public static readonly Output GlobalInstance = new Output(Console.Out, new StreamWriter("Global.log"));

		public readonly TextWriter[] OutStreams;

		public Output (params TextWriter[] outStreams)
		{
			this.OutStreams = outStreams;
		}

		public void Write (String value)
		{
			foreach(TextWriter s in OutStreams)
			{
				s.Write(value);
			}
		}

		public void WriteLine (String value)
		{
			foreach(TextWriter s in OutStreams)
			{
				s.WriteLine(value);
			}
		}
	}
}

