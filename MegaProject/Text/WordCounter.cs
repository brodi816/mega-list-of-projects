using System;

namespace MegaProject
{
	public class WordCounter : ProjectBase
	{
		public static readonly char[] WordSeperators = {' ', ',', '.', ';', '"', '(', ')'};

		public WordCounter (string name)
			: base(name)
		{
		}

		public static bool IsWordSeperator (char c)
		{
			foreach (char s in WordSeperators) {
				if (c == s) {
					return true;
				}
			}
			return false;
		}

		public static int CountWords (string value)
		{
			int wordCount = 0;
			bool validWord = false;

			for (int i = 0; i < value.Length; i++) {
				bool wordSeperator = IsWordSeperator(value[i]);

				if(validWord && wordSeperator) {
					validWord = false;

				} else if(!validWord && !wordSeperator) {
					validWord = true;
					wordCount++;
				}
			}

			return wordCount;
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);

			if (!args.ContainsKey ("i")) {
				Output.GlobalInstance.WriteLine("Define input string with -i input");
				return;
			}

			string input = args ["i"];

			int wordCount = CountWords(input);

			Output.GlobalInstance.WriteLine(string.Format("{0} words in: {1}", wordCount, input));
		}
	}
}

