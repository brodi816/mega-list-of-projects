using System;
using System.Collections.Generic;

namespace MegaProject
{
	class MainClass
	{
		public static ProjectBase[] programs = {
			new ReverseString("Reverse"),
			new PigLatin("PigLatin"),
			new VowelCounter("VowelCounter"),
			new Palindrome("Palindrome"),
			new WordCounter("WordCounter"),
			new KeyGen("KeyGen"),
			new ChangeReturn("ChangeReturn"),
			new TileCost("TileCost"),
			new TextEditor("TextEditor")
		};

		public static ProjectBase GetProject (string name)
		{
			string loweredName = name.ToLower();
			foreach (ProjectBase project in programs) {
				if(project.Name.ToLower() == loweredName) {
					return project;
				}
			}
			return null;
		}

		public static Dictionary<string, string> DecodeArgs (string[] args)
		{
			Dictionary<string, string> decodedArgs = new Dictionary<string, string> ();
			string currentArgName = null;


			for(int curIndex = 0; curIndex < args.Length; curIndex++) {
				//Checking for arguments
				if (currentArgName == null) {
					if (args [curIndex].StartsWith ("--")) {
						currentArgName = args [curIndex].Substring (2);
					} else if (args [curIndex].StartsWith ("-") && args [curIndex].Length > 1) {
						currentArgName = args [curIndex] [1].ToString ();
					}
				}
				//Check if the argument value is actually another arg, meaning the argument was empty
				else if (args [curIndex].StartsWith ("-")) {
					decodedArgs [currentArgName] = "";
					currentArgName = null;
				} 
				//Otherwise put the value in the current arg
				else {
					decodedArgs [currentArgName] = args [curIndex];
					currentArgName = null;
				}
			}

			if (currentArgName != null) {
				decodedArgs[currentArgName] = "";
			}

			return decodedArgs;
		}

		public static void Main (string[] args)
		{
			Dictionary<string, string> decodedArgs = DecodeArgs (args);


			foreach (string name in decodedArgs.Keys) {
				Output.GlobalInstance.WriteLine (string.Format ("{0}: '{1}'", name, decodedArgs [name]));
			}

			if (decodedArgs.ContainsKey ("project")) {
				ProjectBase project = GetProject (decodedArgs ["project"]);
				if (project != null) {
					project.Run(decodedArgs);
				} else {
					Output.GlobalInstance.WriteLine(string.Format("Project: {0} not found", decodedArgs["project"]));
				}
			} else {
				Output.GlobalInstance.WriteLine ("No project selected with the --project argument");
			}
		}
	}
}
