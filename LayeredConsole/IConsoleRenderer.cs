using System;

namespace LayeredConsole
{
	public interface IConsoleRenderer
	{
		LayeredChar[,] Render(ConsoleViewport viewport);
	}
}

