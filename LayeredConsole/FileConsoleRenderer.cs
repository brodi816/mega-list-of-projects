using System;
using System.Collections.Generic;

namespace LayeredConsole
{
	public class FileConsoleRenderer : IConsoleRenderer
	{
		private byte _SpacesInTab = 4;
		private LinkedLineFile _File = null;
		private byte _Layer = 1;

		public FileConsoleRenderer ()
		{
		}

		public LinkedLineFile File
		{
			get {
				return _File;
			} set {
				_File = value;
			}
		}

		public int GetRenderCursorPos(int row, int column)
		{
			LinkedListNode<String> rowNode = File.GetNodeFromRefRow (row);

			int retColumn = 0;
			for (int i = 0; i < column; i++) {
				switch (rowNode.Value [i]) {
				case '\t':
					retColumn += _SpacesInTab;
					break;
				default:
					retColumn++;
					break;
				}
			}

			return retColumn;
		}

		public int GetLocalCursorPos(int row, int column)
		{
			LinkedListNode<String> rowNode = File.GetNodeFromRefRow (row);

			int compColumn = 0;
			for (int i = 0; i < column; i++) {
				switch (rowNode.Value [i]) {
					case '\t':
					compColumn += _SpacesInTab;
					break;
					default:
					compColumn++;
					break;
				}
				if (compColumn >= column) {
					return i;
				}
			}

			return -1;
		}

		public LayeredChar[,] Render(ConsoleViewport viewport)
		{
			LayeredChar[,] ret = new LayeredChar[viewport.Height, viewport.Width];
			if (_File != null) {
				LinkedListNode<String> CurrentNode = _File.GetNodeFromRefRow (viewport.Y);

				if (CurrentNode == null) {
					return ret;
				}

				//loop through all cells in the viewport
				for(int i = 0; i < viewport.Height; i++) {

					int displayX = 0;
					for (int x = viewport.X; x < (viewport.Width + viewport.X); x++) {
						if (x >= CurrentNode.Value.Length) {
							break;
						}

						char c = CurrentNode.Value [x];



						switch (c) {
						case '\t':
							for (int j = 0; j < _SpacesInTab && displayX + j < viewport.Width; j++) {
								ret [i, displayX + j].Char = ' ';
								ret [i, displayX + j].Layer = _Layer;
							}
							displayX += _SpacesInTab;
							break;
						default:
							ret [i, displayX].Char = c;
							ret [i, displayX].Layer = _Layer;
							displayX++;
							break;
						}

						if (displayX >= viewport.Width) {
							break;
						}


					}



					//Skip rest of rows if no more lines exist
					if(CurrentNode.Next == null) {
						break;
					}
					CurrentNode = CurrentNode.Next;
				}
			}

			return ret;
		}
	}
}

