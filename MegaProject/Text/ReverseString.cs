using System;

namespace MegaProject
{
	public class ReverseString : ProjectBase
	{
		public ReverseString (string name)
			: base(name)
		{
		}

		public static string Reverse(string input)
		{
			string reverse = "";
			for(int i = 0; i < input.Length; i++)
			{
				reverse += input[input.Length - i - 1];
			}
			return reverse;
		}

		public override void Run (System.Collections.Generic.Dictionary<string, string> args)
		{
			base.Run (args);

			if (args.ContainsKey ("i")) {
				string input = args["i"];
				string reverse = Reverse(input);

				Output.GlobalInstance.WriteLine(string.Format("Reverse of \"{0}\": \"{1}\"", input, reverse));
			} else {
				Output.GlobalInstance.WriteLine("Define input string with -i input");
			}
		}
	}
}

