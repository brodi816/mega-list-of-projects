using System;
using System.Collections.Generic;

namespace MegaProject
{
	public class VowelCounter : ProjectBase
	{
		private static char[] _Vowels = new char[] {'a', 'e', 'i', 'o', 'u', 'y'};

		public VowelCounter (string name)
			: base(name)
		{
		}

		public static Dictionary<char, int> CountVowels (string value)
		{
			Dictionary<char, int> vowelCount = new Dictionary<char, int> ();

			foreach (char vowel in _Vowels) {
				vowelCount[vowel] = 0;
				foreach (char c in value.ToLower()) {
					if(vowel == c) {
						vowelCount[vowel] += 1;
					}
				}
			}

			return vowelCount;
		}

		public static int GetSum (Dictionary<char, int> vowelCount)
		{
			int sum = 0;
			foreach (int i in vowelCount.Values) {
				sum += i;
			}
			return sum;
		}

		public override void Run (Dictionary<string, string> args)
		{
			base.Run (args);

			if (!args.ContainsKey ("i")) {
				Output.GlobalInstance.WriteLine("Define input string with -i input");
				return;
			}

			string input = args ["i"];

			Dictionary<char, int> vowelCount = CountVowels (input);

			Output.GlobalInstance.WriteLine (string.Format ("Input: {0}", input));
			Output.GlobalInstance.WriteLine (string.Format ("Vowel Sum: {0}", GetSum (vowelCount)));

			Output.GlobalInstance.WriteLine ("Vowel Distribution [");
			foreach (char vowel in vowelCount.Keys) {
				Output.GlobalInstance.WriteLine(string.Format("{0}: {1}", vowel, vowelCount[vowel]));
			}
			Output.GlobalInstance.WriteLine("]");
		}
	}
}

