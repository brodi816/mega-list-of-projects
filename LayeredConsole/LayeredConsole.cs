using System;
using System.Collections.Generic;

namespace LayeredConsole
{
	public class LayeredConsole
	{
		public ConsoleViewport Viewport = new ConsoleViewport (Console.BufferWidth, Console.BufferHeight);
		public List<IConsoleRenderer> Renderers = new List<IConsoleRenderer>();

		public LayeredConsole ()
		{
		}

		public void OutputToConsole()
		{


			LayeredChar[,] output = new LayeredChar[Viewport.Height, Viewport.Width];

			char[] buffer = new char[Viewport.Height * Viewport.Width];


			foreach (IConsoleRenderer renderer in Renderers) {
				LayeredChar[,] layer = renderer.Render (Viewport);
				for (int y = 0; y < Viewport.Height; y++) {
					int bufferY = y * Viewport.Width;

					for (int x = 0; x < Viewport.Width; x++) {
						if (output [y, x].Char == '\0') {
							output [y, x].Char = ' ';
						}

						if (layer [y, x].Char != '\0') {
							if (output [y, x].Layer >= layer [y, x].Layer) {
								continue;
							}

							output [y, x].Char = layer [y, x].Char;
							output [y, x].Layer = layer [y, x].Layer;
						}





						buffer [bufferY + x] = output [y, x].Char;
					}
				}
			}

			int cursorX = Console.CursorLeft;
			int cursorY = Console.CursorTop;
			Console.CursorLeft = 0;
			Console.CursorTop = 0;


			Console.Write(buffer);

			//restore previous cursor position
			Console.CursorLeft = cursorX;
			Console.CursorTop = cursorY;
		}
	}
}

