using System;

namespace LayeredConsole
{
	public class TextLineRenderer : IConsoleRenderer
	{
		private string _Text = "";
		private int _Row = 0;
		private int _Column = 0;

		public TextLineRenderer ()
		{
		}

		public string Text
		{
			get {
				return _Text;
			}
			set {
				_Text = value;
			}
		}

		public int Row
		{
			get {
				return _Row;
			}
			set {
				_Row = value;
			}
		}

		public int Column
		{
			get {
				return _Column
			}
			set {
				_Column = value;
			}
		}

		public void SetPos(int row, int column) {
			_Row = row;
			_Column = column;
		}

		public LayeredChar[,] Render(ConsoleViewport viewport)
		{
			LayeredChar[,] ret = new LayeredChar[viewport.Height, viewport.Width];
		}
	}
}

