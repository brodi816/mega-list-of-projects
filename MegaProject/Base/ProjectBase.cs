using System;
using System.Collections.Generic;

namespace MegaProject
{
	public class ProjectBase
	{
		public readonly string Name;

		public ProjectBase (string name)
		{
			this.Name = name;
		}



		public virtual void Run (Dictionary<string, string> args)
		{
		}
	}
}

